const webpack = require('webpack');

const ExtractTextPlugin = require('extract-text-webpack-plugin');

require('babel-polyfill').default;

const extractCss = new ExtractTextPlugin('css/[name].css', {disable: true});
let baseUrl = process.env.BASE_URL && process.env.BASE_URL.trim() || '/api';

module.exports = {
    devtool: 'cheap-module-eval-source-map',
    entry: {
        public: ['./public/index', 'webpack-hot-middleware/client'],
    },

    output: {
        publicPath: '/static'
    },

    plugins: [
        extractCss,
        new webpack.DefinePlugin({
            'process.env': {
                NODE_ENV: '"development"',
              __APP_BASE_URL__: `'${baseUrl}'`,
            },
            __DEV__: true
        }),
        new webpack.HotModuleReplacementPlugin(),
        new webpack.NoErrorsPlugin()
    ]
};
