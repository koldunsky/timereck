const webpack = require('webpack');
const path = require('path');
const ManifestPlugin = require('webpack-manifest-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');

module.exports = {
  entry: {
    public: './public/index',
    // Styles останется в любом случае как js файл, но extractTextPlugin уберет из него css и
    // вынесет в отдельный стилевой файл.
    styles: './sass/index'
  },

  output: {
    path: path.resolve(__dirname, '../build'),
    filename: '[name].js',
    chunkFilename: '[name].js',
    publicPath: '/static/',
  },

  plugins: [
    new CleanWebpackPlugin(['assets'], {
      root: path.resolve(__dirname, '../build'),
      verbose: true
    }),
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: '"production"'
      },
      __DEV__: false
    }),
    new webpack.optimize.DedupePlugin(),
    new webpack.optimize.UglifyJsPlugin({
      minimize: true,
      compress: {
        warnings: false,
        drop_console: true
      },
      comments: false
    }),
    new ManifestPlugin()
  ]
};
