const path = require('path');
const webpack = require('webpack');
const autoprefixer = require('autoprefixer');
const merge = require('webpack-merge');

const extendedConfig = {
  development: require('./dev.config'),
  production: require('./prod.config'),
  local: require('./local.config'),
};

const ExtractTextPlugin = require('extract-text-webpack-plugin');

require('babel-polyfill').default;

const  TARGET = (process.env.NODE_ENV || 'development').trim();

const PATHS = {
  app: path.join(__dirname, '../src'),
  build: path.join(__dirname, '../static')
};

const assetName = '[name].[ext]';
// const assetName = TARGET === 'prod' ? '[name].[hash].[ext]' : '[name].[ext]';

const extractCss = new ExtractTextPlugin(`css/${assetName.replace(/\[ext]/, 'css')}`);


const common = {
  context: PATHS.app,

  output: {
    path: PATHS.build,
    filename: '[name].js'
  },

  resolve: {
    extensions: ['', '.js', '.scss'],
    modulesDirectories: ['node_modules', PATHS.app],
    alias: {
      tools: path.resolve(__dirname, "../src/sass/_tools")
    }
  },

  module: {
    preLoaders: [{
      test: /\.js$/,
      loaders: ['eslint'],
      include: [
        path.resolve(__dirname, '../src'),
      ]
    }],
    loaders: [{
      test: /\.woff(\?v=\d+\.\d+\.\d+)?$/,
      loader: 'url',
      query: {
        limit: 10000,
        mimetype: 'application/font-woff',
        name: `fonts/${assetName}`
      }
    }, {
      test: /\.woff2(\?v=\d+\.\d+\.\d+)?$/,
      loader: 'url',
      query: {
        limit: 10000,
        mimetype: 'application/font-woff2',
        name: `fonts/${assetName}`,
      }
    }, {
      test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,
      loader: 'url',
      query: {
        limit: 10000,
        mimetype: 'application/octet-stream',
        name: `fonts/${assetName}`
      }
    }, {
      test: /\.otf(\?v=\d+\.\d+\.\d+)?$/,
      loader: 'url',
      query: {
        limit: 10000,
        mimetype: 'application/font-otf',
        name: `fonts/${assetName}`
      }
    }, {
      test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,
      loader: 'file',
      query: {
        limit: 10000,
        name: `fonts/${assetName}`
      }
    }, {
      test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
      loader: 'url?limit=10000&mimetype=image/svg+xml',
      exclude: [
        path.resolve(__dirname, '../src/img/bc_new/')
      ]
    },{
      test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
      loader: 'file?name=[name].[ext]',
      include: [
        path.resolve(__dirname, '../src/img/bc_new/')
      ]
    }, {
      test: /\.js$/,
      loaders: ['babel-loader'],
      exclude: /node_modules/,
    }, {
      test: /\.png$/,
      loader: `file?name=${assetName}`,
    }, {
      test: /\.jpg$/,
      loader: `file?name=${assetName}`,
    }, {
      test: /\.gif$/,
      loader: `file?name=${assetName}`,
    }, {
        test: /\.scss$/,
        loader: ExtractTextPlugin.extract(
          "style",
          "css-loader!postcss-loader!resolve-url-loader!sass-loader?sourceMap")
    }, {
        test: /\.css$/,
        loader: 'style-loader!css-loader!postcss-loader'
    }]
  },

  sassLoader: {
    data: '@import "~tools";'
  },

  plugins: [
    extractCss,
    new webpack.optimize.OccurenceOrderPlugin(),
    new webpack.optimize.CommonsChunkPlugin({
      name: 'common',
      filename: 'common.js',
      minChunks: 3
    })
  ]
};

module.exports = merge(common, extendedConfig[TARGET]);
