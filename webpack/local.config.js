const webpack = require('webpack');
const path = require('path');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

const assetName = '[name].[ext]';

module.exports = {
  entry: {
    public: './public/index',
    // Styles останется в любом случае как js файл, но extractTextPlugin уберет из него css и
    // вынесет в отдельный стилевой файл.
    styles: './sass/index'
  },

  output: {
    path: path.resolve(__dirname, '../build'),
    publicPath: path.resolve('/')
  },

  module: {
    preLoaders: [{
      test: /\.js$/,
      loaders: ['eslint'],
      include: [
        path.resolve(__dirname, '../src'),
      ]
    }],
    loaders: [{
      test: /\.woff(\?v=\d+\.\d+\.\d+)?$/,
      loader: 'url',
      query: {
        limit: 10000,
        mimetype: 'application/font-woff',
        name: `/fonts/${assetName}`
      }
    }, {
      test: /\.woff2(\?v=\d+\.\d+\.\d+)?$/,
      loader: 'url',
      query: {
        limit: 10000,
        mimetype: 'application/font-woff2',
        name: `/fonts/${assetName}`,
      }
    }, {
      test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,
      loader: 'url',
      query: {
        limit: 10000,
        mimetype: 'application/octet-stream',
        name: `/fonts/${assetName}`
      }
    }, {
      test: /\.otf(\?v=\d+\.\d+\.\d+)?$/,
      loader: 'url',
      query: {
        limit: 10000,
        mimetype: 'application/font-otf',
        name: `/fonts/${assetName}`
      }
    }, {
      test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,
      loader: 'file',
      query: {
        limit: 10000,
        name: `/fonts/${assetName}`
      }
    }, {
      test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
      loader: 'url?limit=10000&mimetype=image/svg+xml',
      exclude: [
        path.resolve(__dirname, '../src/img/bc_new/')
      ]
    },{
      test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
      loader: 'file?name=/[name].[ext]',
      include: [
        path.resolve(__dirname, '../src/img/bc_new/')
      ]
    }, {
      test: /\.js$/,
      loaders: ['babel-loader'],
      exclude: /node_modules/,
    }, {
      test: /\.png$/,
      loader: `file?name=/${assetName}`,
    }, {
      test: /\.jpg$/,
      loader: `file?name=/${assetName}`,
    }, {
      test: /\.gif$/,
      loader: `file?name=/${assetName}`,
    }, {
      test: /\.scss$/,
      loader: ExtractTextPlugin.extract(
        "style",
        "css-loader!postcss-loader!resolve-url-loader!sass-loader?sourceMap")
    }, {
      test: /\.css$/,
      loader: 'style-loader!css-loader!postcss-loader'
    }]
  },

  plugins: [
    new CleanWebpackPlugin(['assets'], {
        root: path.resolve(__dirname, '../build'),
        verbose: true
    }),
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: '"local"',
      },
      __APP_BASE_URL__: process.env.BASE_URL,
      __DEV__: true
    }),
    new webpack.NoErrorsPlugin(),
  ]
};
