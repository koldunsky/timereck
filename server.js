const http = require('http');
const express = require('express');
const app = express();
const open = require('open');

(function initWebpack() {
  const webpack = require('webpack');
  const webpackConfig = require('./webpack/common.config');
  const compiler = webpack(webpackConfig);

  app.use(require('webpack-dev-middleware')(compiler, {
    noInfo: true,
    publicPath: webpackConfig.output.publicPath
  }));

  app.use(require('webpack-hot-middleware')(compiler, {
    log: console.log,
    path: '/__webpack_hmr',
    heartBeat: 10 * 1000
  }));

  app.use(express.static(__dirname + '/'));
})();

app.get('/dealer', (req, res) => {
  res.sendFile(__dirname + '/dealer.html');
});

app.get('/importer', (req, res) => {
  res.sendFile(__dirname + '/importer.html');
});

app.get(/.*/, (req, res) => {
  res.sendFile(__dirname + '/index.html');
});

const server = http.createServer(app);
const port = process.env.PORT || '3001';

server.listen(port, () => {
  const address = server.address();
  console.info('Listening on: %j:%d', address, address.port);
});

// Open browser after server launch
open(`http://localhost:${port.trim()}/`);
