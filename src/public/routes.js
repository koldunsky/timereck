import React from 'react';
import { Route, IndexRoute } from 'react-router';
// import modifySettings from './utils/modifySettings';
// import routeHandler from './utils/routeHandler';
import App from './containers/App';
import {APP_ROOT_URL} from './constants/urls';

// pages
// import SelectCar from './pages/SelectCar';
// import Calculator from './pages/Calculator';
// import ChooseDealer from './pages/ChooseDealer';
import NotFound from '../pages/NotFound';
import Index from '../pages/Index';

export default (
  <div>
    <Route path={APP_ROOT_URL} component={App} >
      <IndexRoute component={Index} />
    </Route>

    <Route path='*' component={NotFound} />
  </div>
);
