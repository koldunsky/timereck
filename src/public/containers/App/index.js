import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import classNames from 'classnames';
import { bindActionCreators } from 'redux';
import * as actions from '../../actions/app';
import {getParameterByName} from '../../../utils/helpers';

if(process.env.NODE_ENV === 'development') {
  require('../../../sass/index.scss');
}

class App extends Component {
  static propTypes = {
    app: PropTypes.object.isRequired,
    children: PropTypes.element.isRequired
  };

  static childContextTypes = {
    carObj: PropTypes.object,
  };

  getChildContext() {
    const carObj = this.props.app.car;
    return {carObj: {...carObj}};
  }

  render() {
    const {main, wrapper} = this.props.app;

    return (
      <main className={classNames('page-main', main)} >
        <article className='page-main__content' >
          <div className={classNames('page-main__wrapper',  `page-main__wrapper--${wrapper}` )} >
            <div className='page-main__container' >
              {this.props.children}
            </div>
          </div>
        </article>
      </main>
    );
  }

  componentWillMount() {
    const carId = this.props.params.carId;
    const dealerId =  this.props.params.dealerId || getParameterByName('dealerId', location.href);

    if(typeof dealerId !== 'undefined' && dealerId) {
      this.props.actions.setDealer({id: dealerId, name: 'dealer_name'});
    }
    if(typeof carId !== 'undefined' && carId) {
      this.props.actions.setCar({id: carId});
    }
  }
}

const mapStateToProps = state => ({
  app: state.app
});

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(actions, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(App);
